<?php

namespace App\Acme\SumoImage;

use Illuminate\Support\ServiceProvider;

class SumoImageServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('sumoimage', 'App\Acme\SumoImage\SumoImage');
	}
}