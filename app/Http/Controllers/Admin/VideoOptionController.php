<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoOptionRequest;

use Acme\Facades\Activity;

use App\VideoOption;
use App\Asset;
use App\VideoBanner;
use App\Seo;

class VideoOptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($autoplay = $request->autoplay) {
            $data = VideoOption::where('autoplay', 'LIKE', '%' . $autoplay . '%')->paginate(25);
        } else {
            $data = VideoOption::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/video_options/index')
            ->with('title', 'VideoOptions')
            ->with('menu', 'video_options')
            ->with('keyword', $request->autoplay)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create(Request $request)
    {
        
        $data = $request->all();
        if($request->has('autoplay')){
            $data['autoplay'] = 1;
        }
        dd($data);
        VideoOption::create($data);
        
        return view('admin/video_options/create')
            ->with('title', 'Create video_option')
            ->with('menu', 'video_options');
    }
    
    public function deleteVideoBanner(VideoOptionRequest $request){
        $input = $request->all();
        $deleted = VideoBanner::where('id',$input['id'])->first();
        $delete = $deleted->delete();
        $videos = VideoBanner::where('type', $input['type'])->get();
        $view = view('admin/modals/video-banner-list')
        ->with('videos', $videos)
        ->render();
        $response = [
            'notifTitle'=>'Save Successful',
            'view'=>$view,
            'type'=>$input['type']
        ];
        return $response;
    }

    public function store(VideoOptionRequest $request)
    {
        $input = $request->all();
        $video_option = VideoOption::create($input);
        $input['option_id'] = $video_option['id'];
        $videos = VideoBanner::create($input);
        $type = $videos->type;
        $videos = $videos->where('type', $type)->with('video_option')->get();
        $view = view('admin/modals/video-banner-list')
        ->with('videos', $videos)
        ->render();

        $response = [
            'notifTitle'=>'Save Successful',
            'view'=>$view,
            'type'=>$type
        ];
        return $response;
        // $log = 'creates a new video_option "' . $video_option->name . '"';
        // Activity::create($log);

        // $response = [
        //     'notifTitle'=>'Save successful.',
        //     'notifMessage'=>'Redirecting to edit.',
        //     'resetForm'=>true,
        //     // 'redirect'=>route('adminVideoOptionsEdit', [$video_option->id])
        //     'redirect'=>route('adminBannersCreate')
        // ];

        // return response()->json($response);
    }
    public function youtube(){
        $videos = VideoBanner::where('type', 'youtube')
        ->with('video_option')
        ->get();
        $view = view('admin/modals/video-banner-list')
        ->with('videos', $videos)
        ->render();
        $response = [
            'view'=>$view
            ];
        return response($response);
    }
    public function vimeo(){
        $videos = VideoBanner::where('type', 'vimeo')->get();
        $view = view('admin/modals/video-banner-list')
        ->with('video_option')
        ->with('videos', $videos)
        ->render();
        $response = [
            'view'=>$view
            ];
        return response($response);
    }

    public function library(){
        $videos = VideoBanner::where('type', 'upload')->get();
        $view = view('admin/modals/video-banner-list')
        ->with('video_option')
        ->with('videos', $videos)
        ->render();
        $response = [
            'view'=>$view
            ];
        return response($response);
    }

    public function getUploadedVideos()
    {
        $assets = Asset::where('type','video')
        ->get();
        $view = view('admin/modals/video-uploaded-list')
        ->with('assets', $assets)
        ->render();
        $response = [
            'view'=>$view
            ];
        return response()->json($response);
    }

    public function show($id)
    {
        return view('admin/video_options/show')
            ->with('title', 'Show video_option')
            ->with('data', VideoOption::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/video_options/view')
            ->with('title', 'View video_option')
            ->with('menu', 'video_options')
            ->with('data', VideoOption::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = VideoOption::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/video_options/edit')
            ->with('title', 'Edit video_option')
            ->with('menu', 'video_options')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(VideoOptionRequest $request, $id)
    {
        $input = $request->all();
        $video_option = VideoOption::findOrFail($id);
        $video_option->update($input);

        // $log = 'edits a video_option "' . $video_option->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = VideoOption::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = VideoOption::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->autoplay;
        }
        // $log = 'deletes a new video_option "' . implode(', ', $names) . '"';
        // Activity::create($log);

        VideoOption::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminVideoOptions')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/video_options', array('as'=>'adminVideoOptions','uses'=>'Admin\VideoOptionController@index'));
Route::get('admin/video_options/create', array('as'=>'adminVideoOptionsCreate','uses'=>'Admin\VideoOptionController@create'));
Route::post('admin/video_options/', array('as'=>'adminVideoOptionsStore','uses'=>'Admin\VideoOptionController@store'));
Route::get('admin/video_options/{id}/show', array('as'=>'adminVideoOptionsShow','uses'=>'Admin\VideoOptionController@show'));
Route::get('admin/video_options/{id}/view', array('as'=>'adminVideoOptionsView','uses'=>'Admin\VideoOptionController@view'));
Route::get('admin/video_options/{id}/edit', array('as'=>'adminVideoOptionsEdit','uses'=>'Admin\VideoOptionController@edit'));
Route::patch('admin/video_options/{id}', array('as'=>'adminVideoOptionsUpdate','uses'=>'Admin\VideoOptionController@update'));
Route::post('admin/video_options/seo', array('as'=>'adminVideoOptionsSeo','uses'=>'Admin\VideoOptionController@seo'));
Route::delete('admin/video_options/destroy', array('as'=>'adminVideoOptionsDestroy','uses'=>'Admin\VideoOptionController@destroy'));
*/
}
