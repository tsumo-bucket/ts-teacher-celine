<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ActivityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'log' => 'required|max:191',
            'value_from' => 'required',
            'value_to' => 'required',
            'loggable_id' => 'required|integer',
            'loggable_type' => 'required|max:191',
            'identifier_value' => 'required|max:191',
        ];
    }
}
