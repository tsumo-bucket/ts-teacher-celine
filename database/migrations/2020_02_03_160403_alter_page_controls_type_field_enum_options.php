<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPageControlsTypeFieldEnumOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE page_controls CHANGE COLUMN type type ENUM('text', 'number', 'checkbox', 'textarea', 'asset', 'select', 'color', 'date', 'time', 'date_time', 'products', 'tags') NOT NULL DEFAULT 'text'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE page_controls CHANGE COLUMN type type ENUM('text', 'number', 'checkbox', 'textarea', 'asset', 'select', 'color', 'date', 'time', 'date_time') NOT NULL DEFAULT 'text'");
    }
}
