<translatable object-class="App\PageControl">
	<div class="row">
		<div class="col-md-7">
			<div class="caboodle-card">
				<div class="caboodle-card-body">
					@include('admin.pages.partials.controls')
				</div>
			</div>
		</div>
	</div>
	<div id="hiddenImageSelector" class="caboodle-form-group sumo-asset-select hide">
        <label for="custom_value with-tooltip-helper">
            Image
            <i
                class="fas fa-question-circle text-primary tooltip-helper"
                data-toggle="tooltip"
                title="Display image."
            ></i>
        </label>
        
        {!! Form::hidden('fake_image', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id, 'data-thumbnail'=>'image_thumbnail']) !!}
    </div>
	<input type="hidden" name="controls" val="">
	<input type="hidden" name="translatables" value="title,caption,button_text,image" />
</translatable>

@section('added-scripts')
	@parent
	<script>
		var controls = [];
		$(document).ready(function() {
			$('.date-picker').datetimepicker({
				format: 'MMM DD, YYYY'
			});
		});
		
		$.each($('.control-group'), function() {
			controls.push($(this).attr('control'));
		});
    // alert(controls);
		$('[name="controls"]').val(controls);
	</script>
@stop

<?php
//
?>